# I/O Exercises
#
# * Write a `guessing_game` method.
def guessing_game
    chosen_num = (1..100).to_a.sample

    puts "Guess a number"
    number = gets.chomp

    num_guesses = 0

    until number.to_i == chosen_num
      if number.to_i > chosen_num
        print "#{number}.. too high!"
        number = gets.chomp
      elsif number.to_i < chosen_num
        print "#{number}..too low"
        number = gets.chomp
      end
      num_guesses += 1
    end
    print chosen_num
    print num_guesses
    "correct! the number was #{chosen_num} and it took #{num_guesses} guesses"
end
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
